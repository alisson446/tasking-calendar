const fs = require('fs');
const { calendars, events, channels } = require('./src');
const authorize = require('./lib/authorizeToken');

// Load client secrets from a local file.
fs.readFile('client_secret.json', function processClientSecrets(err, content) {
  if (err) {
    console.log('Error loading client secret file: ' + err);
    return;
  }
  // Authorize a client with the loaded credentials, then call the
  // Google Calendar API.
  authorize(JSON.parse(content)).then(auth => {
    calendars.listCalendars(auth);
  });
});
