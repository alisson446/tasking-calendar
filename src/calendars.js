const calendar = require('../lib/calendarApi');

function createCalendar(auth) {

  const newCalendar = {
    summary: 'sumary 1'
  };

  calendar.calendars.insert({
    auth: auth,
    resource: newCalendar
  }, function(err, calendar) {
    if (err) {
      console.log('There was an error contacting the Calendar service: ' + err);
      return;
    }
    console.log('Calendar created: %s', calendar.id);
  });

}

function patchCalendar(auth) {

  calendar.calendars.patch({
    auth: auth,
    calendarId: 'keeptasking.com_llebjsj6hp3venqm1bjdln9ef4@group.calendar.google.com',
    resource: {
      summary: 'calendar 1'
    }
  }, function(err, calendar) {
    if (err) {
      console.log('There was an error contacting the Calendar service: ' + err);
      return;
    }
    console.log('Calendar updated: %s', calendar.htmlLink);
  });

}

function listCalendars(auth) {

  calendar.calendarList.list({
    auth: auth,
    fields: 'items(id, summary)'
  }, function(err, calendars) {
    if (err) {
      console.log('There was an error contacting the Calendar service: ' + err);
      return;
    }
    const calendarItems = calendars.items;
    for(let i = 0; i < calendarItems.length; i++) {
      console.log(calendarItems[i].id, '-',calendarItems[i].summary);
    }
  });

}

function deleteCalendar(auth) {

  calendar.calendars.delete({
    auth: auth,
    calendarId: 'keeptasking.com_3ecsbdpvls6nnusqu5vpcfuokg@group.calendar.google.com'
  }, function(err, calendar) {
    if (err) {
      console.log('There was an error contacting the Calendar service: ' + err);
      return;
    }
    console.log('Calendar deleted');
  });

}

module.exports = {
  createCalendar,
  patchCalendar,
  listCalendars,
  deleteCalendar
}
