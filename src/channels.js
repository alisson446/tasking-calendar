const calendar = require('../lib/calendarApi');
const uuidV1 = require('uuid/v1');

function watchEvents(auth) {

  const webHookProps = {
    id: uuidV1(),
    type: 'web_hook',
    address: 'https://us-central1-tasking-dev-ao.cloudfunctions.net/calendarNotifications/notifications'
  }

  calendar.events.watch({
    auth: auth,
    calendarId: 'primary',
    resource: webHookProps
  }, function(err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    console.log('Calendar are being watched');
    console.log(response);
  });

}


function stopWatch(auth) {

  calendar.channels.stop({
    auth: auth,
    id: '',
    resourceId: ''
  }, function(err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    console.log('Channel are stoped');
  });

}

module.exports = {
  watchEvents,
	stopWatch
}
