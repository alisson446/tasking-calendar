const calendar = require('../lib/calendarApi');
const uuidV1 = require('uuid/v1');

function createEvents(auth) { 

  const event = {
    id: uuidV1(),
    summary: 'Google I/O 2017',
    location: '800 Howard St., San Francisco, CA 94103',
    description: 'A chance to hear more about Google\'s developer products.',
    start: {
      dateTime: '2017-05-03T09:00:00-07:00',
      timeZone: 'America/Los_Angeles',
    },
    end: {
      dateTime: '2017-05-03T17:00:00-07:00',
      timeZone: 'America/Los_Angeles',
    },
    recurrence: [
      'RRULE:FREQ=DAILY;COUNT=2'
    ],
    attendees: [
      {email: 'lpage@example.com'},
      {email: 'sbrin@example.com'},
    ]
  };

  calendar.events.insert({
    auth: auth,
    calendarId: 'primary',
    resource: event
  }, function(err, event) {
    if (err) {
      console.log('There was an error contacting the Calendar service: ' + err);
      return;
    }
    console.log('Event created: %s', event.htmlLink);
  });

}

function patchEvent(auth) {

  calendar.events.patch({
    auth: auth,
    calendarId: 'primary',
    eventId: '6b9ctopc7lsbrukf3b2mltakno_20170504T160000Z',
    resource: {
      summary: uuidV1()
    }
  }, function(err, event) {
    if (err) {
      console.log('There was an error contacting the Calendar service: ' + err);
      return;
    }
    console.log('Event updated: %s', event.htmlLink);
  });

}

function listEvents(auth, currentPageToken) {

  calendar.events.list({
    auth: auth,
    calendarId: 'primary',
    // timeMin: (new Date()).toISOString(),
    pageToken: currentPageToken || null,
    maxResults: 3,
    singleEvents: true,
    fields: 'nextPageToken, items(id, summary)',
    orderBy: 'startTime'
  }, function(err, response) {

    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }

    const events = response.items;
    if (events.length == 0) {
      console.log('No upcoming events found.');
    } else {
      for (let i = 0; i < events.length; i++) {
        const event = events[i];
        console.log('%s - %s', event.id, event.summary);
      }
    }

    const nextPageToken = response.nextPageToken;
    if(nextPageToken != null) {
      listEvents(auth, nextPageToken);
    }
  });

}

module.exports = {
  createEvents,
  patchEvent,
  listEvents
}
