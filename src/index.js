module.exports = {
  calendars: require('./calendars'),
  channels: require('./channels'),
  events: require('./events')
};
